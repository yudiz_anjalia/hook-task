import React, { useState, useEffect } from 'react';

const UseEffect = () => {
    const [count , setCount] = useState(0);
     
    useEffect(() => {
        if(count >= 1) {
            document.title = `Chats (${count})`
        }
        else {
            document.title = `Chats`
        }
    });

    console.log("Hello People");
    return (
        <div>
            <h1>{count} Message in your Chatbox</h1>
            <button onClick={() => setCount(count + 1)}>Click here</button>
        </div>
    )
}

export default UseEffect;