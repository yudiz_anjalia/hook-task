import React , {useContext} from 'react'
import { BioData } from './ComA';

const ComC = ({name}) => {
    const happy = useContext(BioData);
  return (
    <h1>Welcome To the world of  {happy} </h1>
  )
}

export default ComC