import React , { useState , useRef} from 'react'

const Uncontrolled = () => {


    const luckyNumber = useRef(null);
    const [show , setShow] = useState(false);

    const submitForm = (e) => {
        e.preventDefault();
        const number = luckyNumber.current.value;
        number ===" " ? alert ('please fill something') : setShow(true);
     
    }

  return (
      <div>
          <form action='' onSubmit = {submitForm}>
              <div>
                  <label htmlFor='luckyNumber'> <h1>Enter your Lucky Number</h1></label>
                  <input type = "number" id = "luckyNumber" ref={luckyNumber}></input>
              </div>
              <br/>
            <button>Submit</button>
          </form>
          <p>{show ? `Your Lucky Number is ${luckyNumber.current.value}`: "" }</p>
      </div>
  )
}

export default Uncontrolled