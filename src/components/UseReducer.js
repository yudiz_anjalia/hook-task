import React , {useReducer} from 'react'

const initialstate = 0;

const reducer = (state , action) => {
    console.log(state,action);
    if(action.type === "INCREMENT") {
        return state + 1;
    }
    if(action.type === "DECREMENT") {
        return state -1;
    }
}



const UseReducer = () => {

    const [state , dispatch] = useReducer(reducer , initialstate);

  return (
      <>
      <div>
          <p>{state}</p>
          <div >
              <button onClick={() => dispatch({type: "INCREMENT"})}>INCREMENT</button>
              <button onClick={() => dispatch({type: "DECREMENT"})}>DECREMENT</button>
          </div>
      </div>
      </>
    
  )
}

export default UseReducer