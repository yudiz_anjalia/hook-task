import './App.css';
import { useEffect } from 'react';
import UseState from './components/UseState';
import UseEffect from './components/UseEffect';
import Uncontrolled from './components/Uncontrolled';
import UseReducer from './components/UseReducer';
import ComA from './components/UseContext/ComA';

import ReactGA from 'react-ga';

const TRACKING_ID = "UA-226089871-3"; // OUR_TRACKING_ID

ReactGA.initialize(TRACKING_ID);

 
function App() {

  const throwKnownError = () => {
    throw new Error("Sentry Error");
}

  useEffect(() => {
    ReactGA.pageview(window.location.pathname + window.location.search);
  }, []);

  return (
   
    <div className='hook'>
      <h1>Hooks Task</h1>
      <div className='state'>
        <h1>UseState</h1>
        <UseState/></div>
      <div className='effect'>
      <h1>UseEffect</h1>
      <UseEffect/></div>
      <div className='ref'>
      <h1>UseRefernec</h1>
      <Uncontrolled /></div>
      <div className='red'>
      <h1>UseReducer</h1>
      <UseReducer /></div>
      <div className='context'>
      <h1>Usecontext</h1>
      <ComA/></div>
      <div>
            <button onClick={throwKnownError}>Error Button</button>
        </div>
    </div>
    
  );
}

export default App;
