import { useEffect } from 'react'

function UseDarkBody() {
  useEffect(() => {
    document.body.style.backgroundColor = '#282c34'

    return () => {

      <h1>
        Hello
      </h1>
      document.body.style.backgroundColor = '#fff'
    }
  })
}

export default UseDarkBody